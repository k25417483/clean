export class Wheather {
  records!: Records;
}

export class Records {
  locations!: Array<Location>;
}

export class Location {
  location!: Array<WeatherElement>;
}

export class WeatherElement {
  weatherElement!: Array<Time>;
}

export class Time {
  time!: Array<Item>
}

export class Item {
  elementValue!: Array<elementValue>;
  endTime!: Time;
  startTime!: Time;
}

export class elementValue {
  minValue!: number;
  value!: number;
}
