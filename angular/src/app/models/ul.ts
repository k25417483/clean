export class ul {
  records!: Records;
}

export class Records {
  weatherElement!: Array<Time>;
}

export class Location {
  location!: Array<Time>;
}

export class Time {
  time!: Array<Item2>
}

export class Item2 {
  elementValue!: Array<location>;
  dataTime!: Time;
}

export class location {
  locationCode!: number;
  value!: number;
}
