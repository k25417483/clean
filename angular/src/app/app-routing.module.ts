import { FirstComponentComponent } from './compoent/first-component/first-component.component';
import { TryingComponent } from './compoent/trying/trying.component';
import { RegisterComponent } from './compoent/register/register.component';
import { QnaComponent } from './compoent/qna/qna.component';
import { LoginComponent } from './compoent/login/login.component';
import { HomeComponent } from './compoent/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';




const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'qna', component: QnaComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: HomeComponent },
  { path: 'test', component: TryingComponent },
  { path: 'FirstComponent', component: FirstComponentComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
