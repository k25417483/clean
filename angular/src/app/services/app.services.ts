import { User_login } from 'src/app/models/user_login';
import { User } from './../../../../nestjs/src/user/modles/user';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { NgSelectOption } from '@angular/forms';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2'
import { User_register } from '../models/user_register';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  urlloc = 'http://localhost:3000';
  constructor(
    private http: HttpClient
  ) { }


  swalwaeningDialog(msg:string | undefined){
    Swal.fire({
      title: msg ,
      icon:'warning'
    })
  }


  swalsuccessDialog(msg:string | undefined){
    Swal.fire({
      title: msg ,
      icon:'success'
    })
  }

  getApi(): Observable<any> {
    const url = 'https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-D0047-075?Authorization=CWB-475B7FC5-55BB-4317-BF76-62876DBB8A74&locationName=%E5%8C%97%E5%8D%80&elementName=PoP12h,T';
    return this.http.get(url);
  }

  getuln(): Observable<any> {
    const url = 'https://opendata.cwb.gov.tw/api/v1/rest/datastore/O-A0005-001?Authorization=CWB-B3EDB8EC-6AAF-49BD-BF91-3B48BF093901&offset=0&locationCode=466920,467490';
    return this.http.get(url);
  }

  getorderlist(): Observable<any> {
    const url = this.urlloc +'/order';
    return this.http.get(url);
  }

  register(body : User_register): Observable<any> {
    const url = this.urlloc +'/login/register';
    return this.http.post(url,body);
  }

  login(body : User_login): Observable<any> {
    const url = this.urlloc +'/login/login';
    return this.http.post(url,body);
  }


  getuser(loginId: number):Observable<any>{
    const url = this.urlloc +'/login/'+loginId;
    return this.http.get(url);
  }

  getuserlist():Observable<any>{
    const url = this.urlloc +'/login';
    return this.http.get(url);
  }



}
