import { AppService } from 'src/app/services/app.services';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import { User_register } from 'src/app/models/user_register';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup = new FormGroup({
    account: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    mail: new FormControl('',Validators.required),
  });

  constructor(
    private router: Router,
    private appService: AppService,
  ) { }

  ngOnInit(): void {
  }

  Register() {
    this.appService.register({ ...this.registerForm.value }).subscribe((res: User_register) => {
      if (res.code == 400) {
        this.appService.swalwaeningDialog(res.msg)
      } else {
        Swal.fire({
          title: "註冊成功",
          icon: "success"
        }).then(res => {
          this.router.navigate(['/login'])
        })
      }
    })
  }

}


