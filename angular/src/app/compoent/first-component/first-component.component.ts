import { userlist } from './../../models/user_list';
import { Component, OnInit } from '@angular/core';
import { Item1, rain } from 'src/app/models/rain';
import { Item2, ul } from 'src/app/models/ul';
import { User_login } from 'src/app/models/user_login';
import { Item, Wheather } from 'src/app/models/weather';
import { AppService } from 'src/app/services/app.services';

@Component({
  selector: 'app-first-component',
  templateUrl: './first-component.component.html',
  styleUrls: ['./first-component.component.scss']
})
export class FirstComponentComponent implements OnInit {

  wheather: Array<Item> = [];
  rain:Array<Item1> = [];
  ul:Array<Item2> = [];
  user:Array<userlist> = [];

  loginId = JSON.parse(localStorage.getItem('loginId')!);

  constructor(
    private appService: AppService
  ) { }

  ngOnInit(): void {
    this.getList();
    this.getrain();
    this.getul();
    this.getlist();
  }

  getList(): void {
    this.appService.getApi().subscribe((res: Wheather) => {
      console.log(res);
      this.wheather = res.records.locations[0].location[0].weatherElement[1].time;
    })
  }
  getrain(): void {
    this.appService.getApi().subscribe((res: rain) => {
      console.log(res);
      this.rain = res.records.locations[0].location[0].weatherElement[0].time;

    })
  }

  getul(): void {
    this.appService.getuln().subscribe((res: ul) => {
      console.log(res);
      this.ul = res.records.weatherElement[0].time;

    })
  }

  getlist():void{
    this.appService.getuserlist().subscribe((res:Array<userlist>) => {
      console.log(res);
      this.user = res;
    })
  }
}

