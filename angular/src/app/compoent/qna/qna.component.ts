import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-qna',
  templateUrl: './qna.component.html',
  styleUrls: ['./qna.component.scss']
})
export class QnaComponent implements OnInit {
  qnaForm: FormGroup = new FormGroup({
    quest: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
  });

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }
  button() {
    console.log(this.qnaForm.value);
    if (this.qnaForm.get('quest')?.value != '' && this.qnaForm.get('email')?.value != '') {
      this.router.navigate(['/login'])
    } else {
      console.log('請輸入資料');
    }
  }
}
