import { User_login } from 'src/app/models/user_login';
import { AppService } from './../../services/app.services';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  loginId = JSON.parse(localStorage.getItem('loginId')!);
  account = JSON.parse(localStorage.getItem('account')!);

  constructor(
    private router: Router,
    private appService: AppService,
  ) { }

  ngOnInit(): void {
   this.showUser();
  }


showUser():void{
   this.appService.getuser(this.loginId).subscribe((res:User_login)=>{
     this.appService.swalsuccessDialog(res.account + '歡迎');
     this.account = res.account;
   })
 }

}
