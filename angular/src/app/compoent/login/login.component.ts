import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User_login } from 'src/app/models/user_login';
import { AppService } from 'src/app/services/app.services';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']

})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({
    mail: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });
  constructor(
    private router: Router,
    private appService: AppService,
  ) { }



  ngOnInit(): void { }

  login() {
    this.appService.login({ ...this.loginForm.value }).subscribe((res: User_login) => {
      if (res.code == 400) {
        this.appService.swalwaeningDialog(res.msg)
      } else {
        localStorage.setItem('loginId',res.loginId.toString())
        Swal.fire({
          title: "登入成功",
          icon: "success",
        }).then(res => {

          this.router.navigate(['/home'])
        })
      }
    })
  }

}


