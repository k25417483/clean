import { Column, DataType, Model, Table } from 'sequelize-typescript';

@Table({
  tableName: 'login',
  comment: '註冊者',
})
export class loginEntity extends Model<loginEntity> {
  @Column({
    comment: 'login主鍵',
    autoIncrement: true,
    primaryKey: true,
  })
  loginId: number;

  @Column({
    comment: '姓名',
    type: DataType.STRING(20),
  })
  name: string;

  @Column({
    comment: '使用者名稱',
    type: DataType.STRING(20),
  })
  account: string;

  @Column({
    comment: '電子郵件',
    type: DataType.STRING(50),
  })
  mail: string;

  @Column({
    comment: '密碼',
    type: DataType.STRING(50),
  })
  password: string;

  @Column({
    comment: '國籍',
    type: DataType.STRING(100),
  })
  nationality: string;

  @Column({
    comment: '手機',
    type: DataType.STRING(20),
  })
  phone: string;
}
